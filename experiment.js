"use strict"
var page = require('webpage').create();
var server = require('webserver').create();
var system = require('system');
var BASE_URL = "https://www.pasarpolis.com/asuransi/";
var host, port;

if (system.args.length !== 2) {
    console.log('Usage: server.js <some port>');
    phantom.exit(1);
} else {
    port = system.args[1];
    var listening = server.listen(port, function (request, response) {
        var url = request.url;
        var atype = url.indexOf("/")+1;
        var ztype = (url.indexOf("?") != -1) ? url.indexOf("?")-url.indexOf("/") : url.length - url.indexOf("/");
        var type = url.substring(atype,ztype);
        
        // var params = {};
        // var t_url = url.substring(url.indexOf("?") + 1);
        // while(t_url.indexOf("=") != -1){

        // 	var akey = 0;
        // 	var zkey = t_url.indexOf("=");
        // 	var key = t_url.substring(akey,zkey);

        // 	console.log(t_url);
        // 	var avalue = t_url.indexOf("=")+1;
        // 	var zvalue = (t_url.indexOf("&") != -1) ? (t_url.indexOf("&")) : (t_url.length);
        // 	var value = t_url.substring(avalue,zvalue);
        // 	params[key] = value;
        // 	if(t_url.indexOf("&") != -1){
        // 		t_url = t_url.substring(t_url.indexOf("&")+1);
        // 	}
        // 	else{
        // 		t_url = "";
        // 	}
        // }

        var params_url = url.substring(url.indexOf("?")+1);

        var SERVICE_URL = BASE_URL+type+"/step/2?"+params_url; // Turns Out, Every Type Has Different URL METHOD (POST, GET) TODO
        console.log("Service : "+SERVICE_URL);

        page.open(SERVICE_URL,function(status){
        	
			if(status == "success"){
				var products = page.evaluate(function(){
					return products;
				});

				response.write(JSON.stringify(products));
			}
			else{
				var errors = {
					code : 500,
					message : "Internal Server Error",
				};
				response.write(JSON.stringify(errors));
			}
			response.close();	
		});
    });
}